<?php

$id = isset($block['anchor']) && $block['anchor'] ? $block['anchor'] : $block['id'];
$css = isset($block['className']) ? $block['className'] : '';

$title = get_field('title') ? get_field('title') : get_field('name', 'options');
$subtitle = !get_field('subtitle')['type'] ? (get_field('subtitle')['text'] ?: get_field('desc', 'options')) : get_field('subtitle')['image'];

$video = get_field('video');
$image = get_field('image');
$alpha = get_field('alpha');

$isScrollIndicator = get_field('scroll_indicator');

?>

<section id="<?= $id ?>" class="jumbotron <?= $css ?>">

    <?php if($video): ?><video autoplay muted loop src="<?= $video ?>"></video><?php endif; ?>
    <?= acf_img($image, 'large', 'bkg-splash') ?>
    <div class="greyscale" style="opacity: <?= $alpha ?>"></div>

    <div class="container">
        <div class="jumbotron-content">
           <h1><?= $title ?></h1>
            <?php if(!is_array($subtitle)){
                echo '<p class="subtitle">'.$subtitle.'</p>';
            }
            else{
                echo acf_img($subtitle, 'large', 'subtitle');
            } ?>
        </div>
    </div>

    <?php if($isScrollIndicator): ?>
        <div class="scroll-indicator"></div>
    <?php endif; ?>

</section>